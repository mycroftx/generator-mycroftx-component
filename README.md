# generator-mycroftx-component

> [Yeoman](http://yeoman.io) generator


## Getting Started

### Yeoman Generators

To install generator-mycroftx-component from npm, run:

```
$ npm install -g generator-mycroftx-component
```

Finally, initiate the generator:

```
$ yo mycroftx-component
```
## License

MIT
