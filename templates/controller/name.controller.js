(function() {
    'use strict';

    angular.module('<%= scriptAppName %>')
        .controller('<%= classedName %>Ctrl', <%= classedName %>Ctrl);

    /* @ngInhect */
    function <%= classedName %>Ctrl($scope, $log) {
        $scope.message = 'Hello';
    }
})();
